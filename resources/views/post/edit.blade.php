@extends('layouts.app')
@section('judul')
edit
@endsection
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Edit post</div>
            <div class="panel-body">
              <form class="" action="{{ route('post.update', $post) }}" method="post">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-group">
                  <label for="">Title</label>
                  <input type="text" class="form-control" name="title" placeholder="Masukan judul.." value="{{ $post->title }}">
                </div>
                <div class="form-group">
                  <label for="">Category</label>
                  <select class="form-control" name="category_id">
                    @foreach($categories as $category)
                      <option value="{{$category->id}}"
                        @if ($category->id === $post->category_id)
                          selected
                        @endif
                        >{{$category->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label for="">Content</label>
                  <textarea name="content" rows="5" class="form-control" placeholder="Masukan content..">{{ $post->content }}</textarea>
                </div>
                <div class="form-group">
                  <input type="submit" name="" class="btn btn-primary" value="SIMPAN">
                </div>
              </form>
            </div>
        </div>

      </div>
    </div>
  </div>
@endsection
